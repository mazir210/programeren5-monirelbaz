function TableSort(id) {
    // When calling an object constructor or any of its methods,
    // this’ refers to the instance of the object
    // much like any class-based language
    this.tblElelment = document.getElementById(id);
    if (this.tblElelment && this.tblElelment.nodeName == "TABLE") {
        this.prepare();
    }
}
TableSort.prototype.prepare = function() {
    // add arrow up
    // default is ascending order

    var headings = this.tblElelment.tHead.rows[0].cells;
    // headings is een htmlcollection

    for (let i = 0; i < headings.length; i++) {
        headings[i].innerHTML = headings[i].innerHTML + '<span>&nbsp;&nbsp;&uarr;</span>';
        headings[i].className = 'asc';

    }

    this.tblElelment.addEventListener("click", recap(this), false);
}
function recap(test) {

    return function(event2) {
        test.evntHandler(event2);
    }
}
window.onload = function() {
    var jommeke = new TableSort("jommeke");
    var fruit = new TableSort("fruit");
}
TableSort.prototype.evntHandler = function(event) {
    if (event.target.tagName === 'TH') {

        this.sortColumn(event.target);
    }
    else if (event.target.tagName === 'SPAN') {
        if (event.target.parentNode.tagName === 'TH') {
            if (event.target.parentNode.className == "asc") {
                event.target.parentNode.className = 'desc';
                event.target.innerHTML = "&nbsp;&nbsp;&darr;"
            } else {
                event.target.parentNode.className = 'asc';
                event.target.innerHTML = "&nbsp;&nbsp;&uarr;"
            };
        }
    }
}
TableSort.prototype.sortColumn = function(headerCell) {
    // Get cell data for column that is to be sorted from HTML table

    let rows = this.tblElelment.rows;
    let alpha = [],
        numeric = [];
    let alphaIndex = 0,
        numericIndex = 0;
    let cellIndex2 = headerCell.cellIndex;
    for (var i = 1; rows[i]; i++) {
        let cell = rows[i].cells[cellIndex2];
        let content = cell.textContent ? cell.textContent : cell.innerText;
        let numericValue = content.replace(/(\$|\,|\s)/g, "");
        if (parseFloat(numericValue) == numericValue) {
            numeric[numericIndex++] = {
                value: Number(numericValue),
                row: rows[i]
            }
        }
        else {
            alpha[alphaIndex++] = {
                value: content,
                row: rows[i]
            }
        }
    }
    let orderdedColumns = [];
    numeric.sort(function(a, b) {
        return a.value - b.value;
    });
    alpha.sort(function(a, b) {
        let aName = a.value.toLowerCase();
        let bName = b.value.toLowerCase();
        if (aName < bName) {
            return -1
        } else if (aName > bName){
            return 1;
        } else {
            return 0;
        }
    });
    orderdedColumns = numeric.concat(alpha);
    let tBody = this.tblElelment.tBodies[0];
    for (let i = 0; orderdedColumns[i]; i++) {
        tBody.appendChild(orderdedColumns[i].row);
    }
}