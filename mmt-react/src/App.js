import React from 'react'
import hotelData from "./data/hotelsDb.json";
import ArticleHeader from "./components/ArticleHeader";
import ArticleOverview from "./components/ArticleOverview";
import ArticleDetail from "./components/ArticleDetail";


import './App.css';

class App extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
      overview: true,
      selected: {},
    username: "anonymous",
    results: hotelData.hotelsLijst,
      
    };
  }
  

     showDetail = (item) => {
   
    this.setState({
      overview: false,
      selected: item,
    });
    
  };
 /* showOverview = () => {
    this.setState({
      overview: true,
      article: null,
    });
  }; */
    
    //google login 
      getUserName = (eennaam) => {
    this.setState({
      username: eennaam,
      
      
    });
  }



  render() {
if (this.state.overview) {
  return (
    <div className="App">
      <header className="App-header">
        <ArticleHeader title={hotelData.title} action={this.getUserName} />

       {/*  <Login action={this.getUserName} /> */}
      </header>
      <main>
        <ArticleOverview
          results={this.state.results}
          action={this.showDetail}
        />
        
      </main>
    </div>
  );
  
  } else {
    return (
      <div className="App">
        <header className="App-header">
          <ArticleHeader title={hotelData.title} action={this.getUserName} />
          {/*  <Login action={getUserName} /> */}
        </header>
        <main>
          <ArticleDetail
            article={this.state.selected}
            actionOverview={this.showOverview}
            username={this.state.username}
          />
        </main>
      </div>
    );
    
}


    
  }
}

export default App;
