import React from 'react'
import AButton from './AButton';

function ArticleSummary(props) {
  const showDetail = () => {
    props.action(props.item);
  };
  return (
    <div className="result">
      <img src={props.item.imageTh} alt="{item.Headline}" />
      <AButton caption={props.item.Title} onClick={showDetail} />
   {/*    <h3 onClick={showDetail}> {props.item.Title} </h3> */}
    </div>
  )
}

export default ArticleSummary;
