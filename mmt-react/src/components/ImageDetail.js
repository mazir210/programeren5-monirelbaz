import React from "react";
import Slider from "../components/Slider";

function ImageDetail(props) {
    const images = [
     
         props.article.image1,
         props.article.image2,
         props.article.image3,
         props.article.image4,
        props.article.image5,
     
    ];
    
    

  return (
    
      <div>
          <div style={{ display: 'flex', justifyContent: 'space-between' }} />
          <Slider
            options={{
              autoPlay: 4000,
              pauseAutoPlayOnHover: true,
              wrapAround: true,
              fullscreen: true,
              adaptiveHeight: true,
            }}
          >
            {images.map((image, index) => (
              <div style={{ width: '100%', height: '400px', margin: '0 0.5em' }} key={index}>
                <img src={image} alt="" />
              </div>
            ))}
          </Slider>
        </div>
  );
}
export default ImageDetail;