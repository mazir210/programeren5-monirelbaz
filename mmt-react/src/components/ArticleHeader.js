
import React from "react";
import Login from "./Login";



function ArticleHeader(props) {
    

  return (
    <>
      <img alt={`Logo ${props.title}`} src="../logo.jpg" className="App-logo" />
      <h1>{props.title} <Login  action={props.action} /></h1>
      
    </>
  )
}
export default ArticleHeader;