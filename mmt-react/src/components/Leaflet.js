import React from "react";
import "leaflet/dist/leaflet.css";
import L from 'leaflet';

import anchor from '../assets/anchor1.png';
import { Map, Marker, Popup, TileLayer } from "react-leaflet";


function Leaflet(props) {
  

 const position = [props.lng, props.lat];

  var myIcon = L.icon({
    iconUrl: anchor,
    iconSize: [25, 41],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
  }); 

  return (
    <div className="map">
      <Map center={position} zoom={13} style={{ height: "350px" }}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={position} icon={myIcon}>
          <Popup>
            A pretty CSS3 popup.
            <br />
            Easily customizable.
          </Popup>
        </Marker>
      </Map>
    </div>
  );


}
export default Leaflet;

/* render(map, document.getElementById("map-container"));  */
