import React from 'react'

import Leaflet from "../components/Leaflet";
import ImageDetail from "../components/ImageDetail";
import Description from "./Description";
import Comments from "./Comments";

import AButton from "./AButton";
import Likepanel from './Likepanel';


class ArticleDetail extends React.Component {

  
   refreshPage() {
    window.location.reload(false);
  }

render(){
  return (
    <>
      <AButton caption="Terug naar overzicht" onClick={this.refreshPage} />
      <div className="first">
        <div className="images">
          <ImageDetail article={this.props.article} />
        </div>
        <div className="mapdescriptions">
          <Leaflet
            lat={this.props.article.latitude}
            lng={this.props.article.longitude}
          />
          <Description article={this.props.article} />
        </div>
      </div>
      <span>
        <Likepanel keyValue={this.props.article.Id} />
        <h3 id="commentserror" >comments</h3>
        <Comments
          username={this.props.username}
          keyValue={this.props.article.Id}
        />
      </span>
    </>
  )
}
}
export default ArticleDetail;
