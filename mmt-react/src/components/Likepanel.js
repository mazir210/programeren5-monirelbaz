import React from 'react';
import { AiTwotoneLike } from "react-icons/ai";
import axios from "axios";
import Alabel from './Alabel';

class Likepanel extends React.Component {
  host = "http://localhost:5000/MmtLike/";

  constructor(props) {
    super(props);
    this.state = {
      likes: 0,
    };
    this.getLikes();
  }
  getLikes = async () => {
    const keyValue = this.props.keyValue;
     await axios
      .get(`${this.host}${keyValue}`)
      .then(({ data }) => this.setState({ likes: data.likes }))
    .catch((error) => {
                  
                  let errordisplay = document.getElementById("likeserror");
        errordisplay.textContent = "fout bij het laden van likes van de server ";
                });
  };
    addLike = () => {
     
    let nieuwlike = {
      Key: this.props.keyValue,
      name: "niet nodig",
      like: 1,
    };
    axios.post(`http://localhost:5000/MmtLike/`, nieuwlike).then((res) => {
     
      this.getLikes();
    });
  };

  render() {
    return (
      <div>
        <Alabel caption={this.state.likes} />

        <AiTwotoneLike
          color="#63abe6"
          size="30px"
          onClick={() => this.addLike()}
        />
        <span id="likeserror"></span>
      </div>
    );
  }
}

export default Likepanel;
