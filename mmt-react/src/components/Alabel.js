import React from 'react'

export default function Alabel(props) {
     const style = {
       display: "inline-block",
       color: "#ffffff",
       backgroundColor: "#3d94f6",
       textShadow: "0px 1px 0px #1570cd",
       padding: "6px 24px",
       borderRadius: "6px",
       border: "1px solid #337fed",
     };
   
    return (
      <>
          <label style={style}>{props.caption}</label>
      </>
    )
}
