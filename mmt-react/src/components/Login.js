import React from "react";
import GoogleLogin from "react-google-login";


function Login(props) {
  
 const responseGoogle = (response) => {
    
  props.action(response.profileObj.name);
};  

return (
  <GoogleLogin
    clientId="569195582689-e5i9dgaheiipmqkg5dpp2lnt8kfqt866.apps.googleusercontent.com"
    buttonText="Login"
    onSuccess={responseGoogle}
    onFailure={responseGoogle}
    cookiePolicy={"single_host_origin"}
    isSignedIn={true}
  />
);


}



export default Login;