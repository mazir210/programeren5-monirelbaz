import React from 'react'
import ArticleSummary from './ArticleSummary'

function ArticleOverview(props) {
  return (
    <section className="results">
      {props.results.map((item) => (
        <ArticleSummary key={item.Id} item={item} action={props.action} />
      ))}
    </section>
  )
}

export default ArticleOverview;
