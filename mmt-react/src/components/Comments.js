import React from 'react'
import Slider from "../components/Slider";
import axios from "axios";
import AButton from './AButton';

export class Comments extends React.Component {
         constructor(props) {
           super(props);
           this.state = {
             keyValue: parseInt(props.keyValue),
             comments: [],
             newcomment: "",
           };
           this.getComments();
         }

  getComments = async () => {
           
                 await axios
                  .get(
                    `http://localhost:5000/mmtComment/${this.state.keyValue}`
                  )
                  .then(({ data }) => this.setState({ comments: data }))
    
      .catch((error) => {
                  
                  let errordisplay = document.getElementById("commentserror");
        errordisplay.textContent = "fout bij het laden van comments van de server ";
                });
         };

         //add comment
         style = {
           new_comment_input: {
             fontSize: "16px",
             padding: "8px",
             border: "none",
             borderBottomColor: "#ebf3ff",
             borderBottomStyle: "none",
             borderBottomWidth: "medium",
             borderBottom: "2px solid #ddd",
             borderRadius: "8px",
             backgroundColor: "#ebf3ff",
             width: "70%",
             outline: "none",
             minHeight: "75px",
             marginTop: "35px",
           },
         };

         addComment = (event) => {
           event.preventDefault();
           
           let nieuwcomment = {
             Key: this.state.keyValue,
             name: this.props.username,
             comment: this.state.newcomment,
           };
           axios
             .post(`http://localhost:5000/mmtComment/`, nieuwcomment)
             .then((res) => {
               
               this.getComments();
               this.setState({ newcomment: "" });
             });
         };

         render() {
           return (
             <>
               <div>
                 <div
                   style={{
                     display: "flex",
                     justifyContent: "space-between",
                   }}
                 />
                 <Slider
                   options={{
                     autoPlay: 4000,
                     pauseAutoPlayOnHover: true,
                     wrapAround: true,
                     fullscreen: true,
                     adaptiveHeight: true,
                   }}
                 >
                   {this.state.comments.map((comm, index) => (
                     <div
                       style={{
                         width: "100%",
                         height: "100px",
                         margin: "0 0.5em",
                         backgroundColor: "#ebf3ff",
                         textAlign: "center",
                       }}
                       key={index}
                     >
                       <p> {comm.comment}</p>
                       <span>by: {comm.name} </span>
                     </div>
                   ))}
                 </Slider>
               </div>
               <form onSubmit={this.addComment}>
                 <input
                   style={this.style.new_comment_input}
                   className="new_comment_input"
                   type="text"
                   value={this.state.newcomment}
                   onChange={(event) =>
                     this.setState({ newcomment: event.target.value })
                   }
                   placeholder="comment"
                   required
                 />
                 <AButton caption="Add comment" />
               </form>
             </>
           );
         }
       }

export default Comments
