import React from "react";
import { FaMapMarkerAlt, FaBed, FaBabyCarriage } from "react-icons/fa";
import { FcServices } from "react-icons/fc";
import { IoIosBed, IoIosPricetag } from "react-icons/io";
import { IconContext } from "react-icons";


function Description(props) {
  const style = {
    reviewall: {
      display: "flex",
      flexDirection: "row",
    },
    scoreBg: {
      background: "#003580",
      color: "#fff",
      alignItems: "center",
      fontSize: "20px",
      fontWeight: "500",
      borderRadius: "6px 6px 6px 0",
      height: "32px",
      width: "32px",
      minWidth: "32px",
    },
    review: {
      color: "#003580",
      fontSize: "19px",
      fontWeight: "500",
      lineHeight: "1",
      marginLeft: "4px",
      marginTop: "8px",
    },
    scoreNr: {
      marginLeft: "15px",
      marginTop: "8px",
      fontSize: "12px",
    },

    descontainer: {
      flex: "1 1 0%",
      flexDirection: "row",

    },

    span: {
      marginLeft: "15px",
      marginTop: "2px",
     
    },
    desccont1: {
      marginLeft: "15px",
      marginTop: "2px",

    },
  };
  return (
    <div className="descontainer" style={style.descontainer}>
      <div className="desccont1" style={style.desccont1}>
        <h3 className="hotelname">{props.article.Title} </h3>
        <hr />
        <div className="reviewall" style={style.reviewall}>
          <div className="scoreBg" style={style.scoreBg}>
            {props.article.scoreBg}
          </div>
          <span className="review" style={style.review}>
            {props.article.review}
          </span>
          <div className="scoreNr" style={style.scoreNr}>
            ( {props.article.scoreNr})
          </div>
        </div>

        <hr />
        <IconContext.Provider
          value={{ color: "green", className: "global-class-name" }}
        >
          <FaMapMarkerAlt color="blue" size="18px" />
          <span className="span" style={style.span}>
            {props.article.adress}
          </span>

          <hr />
          <FcServices />
          <span style={style.span}>{props.article.Type}</span>
          <hr />
          <FaBed />
          <span style={style.span}>{props.article.beds}</span>
          <hr />
          <IoIosBed />
          <span style={style.span}>{props.article.room_link}</span>
          <hr />
          <FaBabyCarriage />
          <span style={style.span}>{props.article.Babysitting_services}</span>
          <hr />
          <IoIosPricetag />
          <span style={style.span}>{props.article.Price}</span>
          <hr />
        </IconContext.Provider>
      </div>
    </div>
  )
}
export default Description;
