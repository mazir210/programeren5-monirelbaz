import React, { useState, useEffect } from 'react'
import hotelData from './data/hotelsDb.json'
import ArticleHeader from "./components/ArticleHeader";
import ArticleOverview from "./components/ArticleOverview";
import ArticleDetail from "./components/ArticleDetail";
import GoogleLogin from "react-google-login";
import ReactDOM from "react-dom";
import Login from "./components/Login";

import './App.css';

function App() {
  const hotellen = hotelData.hotelsLijst;
const [state, setState] = useState({
  selected: {},
  overview: true,
  username: "",
  results: hotelData.hotelsLijst,
});
  const getUserName = (eennaam) => {
    setState((prevState) => {
      return { ...prevState, username: eennaam };
    });
  }
  /* const [name,setName]=useState("anonymous") */
  const responseGoogle = (response) => {
    
     setState((prevState) => {
       return { ...prevState, username: response.profileObj.name };
     });
  /*   setName(response.profileObj.name); */
  };
  
  
/* useEffect(() => {
    const hotellen = hotelData.hotelsLijst;
    
    setState((prevState) => {
      return { ...prevState, results: hotellen };
    });
}, []) */
  
  const showDetail = (item) => {
    console.log("showdetail")
    setState({
      overview: false,
      selected: item,
    });
    
  };
  /* const showDetail = (item) => {
      
    setState({
      selected: item,
      overview: false,
      username:"anonymous",
    });
    console.log(state);
  }; */
  const showOverview = () => {
  
    setState({
      overview: true,
      
      
    });
  };
  if (state.overview) {
  return (
    <div className="App">
      <header className="App-header">
        <ArticleHeader title={hotelData.title} />

        <Login action={getUserName}/>
      </header>
      <main>
        <ArticleOverview results={hotellen} action={showDetail} />;
      </main>
    </div>
  );
  
  } else {
    return (
      <div className="App">
        <header className="App-header">
          <ArticleHeader title={hotelData.title} />
          <Login action={getUserName} />
        </header>
        <main>
          <ArticleDetail
            article={state.selected}
            actionOverview={showOverview}
            username={state.username}
          />
         
        </main>
      </div>
    );
    
}
  
}

export default App;
