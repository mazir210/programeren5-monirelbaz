import React, { useState, useEffect } from "react";
import Slider from "../components/Slider";
import axios from "axios";


function Comments(props) {
const keyValue = parseInt(props.keyValue);
  
  const [comments, setComment] = useState([]);
  const [newcomment, postComment] = useState();
 

  useEffect( () => {
    const fetchData = async () => {
      const results = await axios.get(
        `http://localhost:5000/mmtComment/${keyValue}`
      );

      setComment(results.data);
    };
   fetchData();
  }, []);
// add comment
  let nieuwcomment = {
    Key:props.keyValue,
      name:"",
    comment:{newcomment},
  }
const handleSubmit = (event) => {
  event.preventDefault();
  axios.post(`http://localhost:5000/mmtComment/`, { nieuwcomment })
    .then(res => {
    console.log(res)
  })

};
 /* getComments = async () => {
   const results = await axios.get(
     `http://localhost:5000/mmtComment/${this.state.keyValue}`
   );
   this.setState({ comments: results });
 };
          */

  return (
    <>
      <div>
        <div style={{ display: "flex", justifyContent: "space-between" }} />
        <Slider
          options={{
            autoPlay: 4000,
            pauseAutoPlayOnHover: true,
            wrapAround: true,
            fullscreen: true,
            adaptiveHeight: true,
          }}
        >
          {comments.map((comm, index) => (
            <div
              style={{
                width: "100%",
                height: "100px",
                margin: "0 0.5em",
                backgroundColor: "#ebf3ff",
                textAlign: "center",
              }}
              key={index}
            >
              <p> {comm.comment}</p>
              <span>by: {comm.name} </span>
            </div>
          ))}
        </Slider>
      </div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={newcomment}
          onChange={(event) => postComment(event.target.value)}
          placeholder="GitHub username"
          required
        />
        <button type="submit">Add Comment</button>
      </form>
    </>
  );
}


export default Comments;
