

// Opdracht programmeren 5 les 8
//ik gebruik in deze oefening de date-fns npm package 
//date-fns provides the most comprehensive, yet simple and consistent toolset for manipulating JavaScript dates in a browser & Node.js.
//website :https://date-fns.org/
const datumFormat = require('date-fns') 

// 
const format = datumFormat.format;

//hier geven we de naam van vandaag 
console.log(format(new Date(), "'Today is a' iiii") + '\n');

//we willen de aantal maanden berekenen tussen vandaag en een opgegeven datum 

console.log(datumFormat.formatDistance(new Date(2020, 8, 1) ,new Date(),{
    addSuffix: true})+ '\n');

//formatRelative Represent :the date in words relative to the given base date
//zes dage geleden 
console.log(datumFormat.formatRelative(datumFormat.subDays(new Date(), 6), new Date())+ '\n');


//toDate:Converts datum van vandaag in letters
console.log(datumFormat.toDate(new Date()));





