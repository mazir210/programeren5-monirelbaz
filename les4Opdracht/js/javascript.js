// Set the map variable
const myMap = L.map('map')

// Load the basemap
const myBasemap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
})

// Add basemap to map id
myBasemap.addTo(myMap)

// Set view of the map
myMap.setView([51.19, 4.49], 12)


// Make an XMLHttpRequest to the JSON data
const request = new XMLHttpRequest()

request.open('GET', 'map.json', true)
request.onload = function() {
  // Begin accessing JSON data here
  const data = JSON.parse(this.response)
  
  // Print cafe markers
  const cafes = data.delhaizes.map(cafe => {
    L.marker([cafe.lat, cafe.long])
      .bindPopup(
        `
          <h2>${cafe.name}</h2>
          <p><b>Neighborhood:</b> ${cafe.neighborhood}</p>
          <p><b>Open op Zondag:</b> ${cafe.openZondag}</p>
          <p><b>Type Delhaize:</b> ${cafe.type}</p>
          
        `
      )
      .openPopup()
      .addTo(myMap)
  }
  )


  //begin count 
  
const neighborhoodCount = data.delhaizes.reduce((sums, cafe) => {
  sums[cafe.neighborhood] = (sums[cafe.neighborhood] || 0) + 1
  return sums
}, {})
const postCodeCount = data.delhaizes.reduce((sums, ad) => {
  sums[ad.postcode] = (sums[ad.postcode] || 0) + 1
  return sums
}, {})



// Create a sidebar
const sidebar = document.getElementById('neighborhoods')

// Print all neighborhoods in sidebar
for (let neighborhood in neighborhoodCount) {
  const p = document.createElement('p')

  p.innerHTML = `<b>${neighborhood}</b> : ${neighborhoodCount[neighborhood]}`
  sidebar.appendChild(p)
}
// Print all postcodes  in sidebar
const sidebar1 = document.getElementById('postcodes')
for (let postCode in postCodeCount) {
  const p = document.createElement('p')

  p.innerHTML = `<b>${postCode}</b> : ${postCodeCount[postCode]}`
  sidebar1.appendChild(p)
}




}

request.send()

