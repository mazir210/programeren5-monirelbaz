function ALabel(props) {
  const style = {
    display: "inline-block",
    color: "#ffffff",
    backgroundColor: "#3d94f6",
    textShadow: "0px 1px 0px #1570cd",
    padding: "6px 24px",
    textShadow: "0px 1px 0px #1570cd",
    borderRadius: "6px",
    border: "1px solid #337fed",
  };
  return (
    <div>
      <label style={style}>{props.caption}</label>
    </div>
  );
}

function AButton(props) {
  const style = {
    flex: 1,
    boxShadow: "inset 0px 1px 0px 0px #97c4fe",
    background: "linear-gradient(to bottom, #3d94f6 5%, #1e62d0 100%)",
    backgroundColor: "#3d94f6",
    borderRadius: "6px",
    border: "1px solid #337fed",
    display: "inline-block",
    cursor: "pointer",
    color: "#ffffff",
    fontFamily: "Arial",
    fontSize: "1em",
    fontWeight: "bold",
    padding: "6px 24px",
    textShadow: "0px 1px 0px #1570cd",
  };

  return (
    <button onClick={props.onClick} style={style}>
      {typeof props.caption !== "undefined" ? props.caption : "button"}
    </button>
  );
}

function ImageSummary(props) {
  const style = {
    borderRadius: "6px",
  };
  return (
    <img style={style} alt={`afbeelding van ${props.name}`} src={props.image} />
  );
}

function ImageDetail(props) {
  const style = {
    // width: "25em",
    flex: "1 ",
  };

  return (
    <img style={style} alt={`afbeelding van ${props.name}`} src={props.image} />
  );
}

function UnorderedList(props) {
  return (
    <ul>
      {props.ul.map((item) => (
        <li key={item.key}>{item.key}</li>
      ))}
    </ul>
  );
}

class LikePanel extends React.Component {
  host = "http://localhost:5000/MmtLike";
  constructor(props) {
    super(props);
    this.state = {
      likes: 0,
    };
    this.getLikes();
  }
  getLikes = () => {
    const keyValue = this.props.keyValue;
    
    let count = 0;
    if (typeof keyValue !== "undefined") {
      // als de callback wordt uitgevoerd, is this niet meer in de scope
      // daarom slaan we die op in een constante en geven die met de callback mee
      const self = this;
      const url = `${this.host}/${keyValue}`;
      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          this.setState({
            likes: data.likes,
          });
        });
    }
  };
  postLikes = () => {
    let item = {
      Key: this.props.keyValue,
      Name: "onbelangrijk",
      Likes: 1,
    };

    postData(this.host, item).then((data) => {
      this.setState({
        likes: data.likes,
      });
    });
  };
  incrementLike = () => {
    const keyValue = this.props.keyValue;
    if (typeof keyValue !== "undefined") {
      this.postLikes();
    } else {
      let newCount = this.state.likes + 1;
      this.setState({
        likes: newCount,
      });
    }
  };

  render() {
    return (
      <div>
        <AButton caption="I like" onClick={this.incrementLike} />
        <ALabel caption={this.state.likes} />
      </div>
    );
  }
}
class Comments extends React.Component {
 
  host = "http://localhost:5000/MmtComment";
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      username: "",
      commentText:"",
    };
    this.getComments();
  }
  getComments = () => {
    const keyValue = this.props.keyValue;
    
    let count = 0;
    if (typeof keyValue !== "undefined") {
      // als de callback wordt uitgevoerd, is this niet meer in de scope
      // daarom slaan we die op in een constante en geven die met de callback mee
      const self = this;
      const url = `${this.host}/${keyValue}`;

      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          this.setState({
            comments: data,
          });
        });
    }
  };
  
  postComments = () => {
    let item = {
      Key: this.props.keyValue,
      Name: "onbelangrijk",
      Likes: 1,
    };

    postData(this.host, item).then((data) => {
      this.setState({
        likes: data.likes,
      });
    });
  };
  //het is dit 
    addComment = () => {
    let item = {
      Key: this.props.keyValue,
      name: this.state.username,
      comment: this.state.commentText,
      
    
    };
    postData("http://localhost:5000/MmtComment", item).then((data) => {
       this.setState({
         comments: data,
       });
      
    });

  };

  render() {
    return (
      <>
        <h3>Comments:</h3>
        {this.state.comments.map((comment) => (
          <div className="comment" Key={comment.id}>
            <h4> {comment.name} </h4>
            <p> {comment.comment} </p>
          </div>
        ))}
        <div id="add-comment-form">
          <h3>Add a Comment</h3>
          <label>
            Name:
            <input
              type="text"
              value={this.props.username}
              onChange={(event) =>
                this.setState({ username: event.target.value })
              }
            />
          </label>
          <label>
            Comment:
            <textarea
              rows="4"
              cols="50"
              value={this.props.commentText}
              onChange={(event) =>
                this.setState({ commentText: event.target.value })
              }
            />
          </label>
          <button onClick={() => this.addComment()}>Add Comment</button>
        </div>
      </>
    );
  }
}
const AddCommentForm = ({ keyValue }) => {
 
  const [username, setUsername] = React.useState("");
  const [commentText, setCommentText] = React.useState("");
 
  const addComment = () => {
    let item = {
      Key: keyValue,
      name: username,
      comment: commentText,
      
    
    };
    postData("http://localhost:5000/MmtComment", item).then((data) => {
      
      
    });

  };

  return (
    <div id="add-comment-form">
      <h3>Add a Comment</h3>
      <label>
        Name:
        <input
          type="text"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
        />
      </label>
      <label>
        Comment:
        <textarea
          rows="4
          cols="50"
          value={commentText}
          onChange={(event) => setCommentText(event.target.value)}
        />
      </label>
      <button onClick={() => addComment()}>Add Comment</button>
    </div>
  );
};
function ImageSummary(props) {
  const style = {
    borderRadius: "6px",
  };
  return (
    <img style={style} alt={`afbeelding van ${props.name}`} src={props.image} />
  );
}
image={this.props.item.imageTh}
              image1={this.props.item.image1}
              image2={this.props.item.image2}
              image3={this.props.item.image3}
              image4={this.props.item.image4}
image5 = { this.props.item.image5 }
              
              class ImageSummary2 extends React.Component(props) {
   style = {
    borderRadius: "6px",
  };
   phptos = [
    {
      name='photo1',
      url=props.image1,

    },
    {
      name='photo2',
      url=props.image2,

    },
    {
      name='photo3',
      url=props.image3,

    },
    {
      name='photo4',
      url=props.image4,

    },
    {
      name='photo5',
      url=props.image5,

    }
  ]
  render() {const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

  return (
     <div>
        <h2> Single Item</h2>
      <Slider {...settings}>
        {
          this.phptos.map((photo) => {
            return (
              <div>
                <img src={photo.url}  />
              </div>
            )
          })
        }
          
        </Slider>
      </div>
  );
    }
}