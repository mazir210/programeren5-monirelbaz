
function ArticleHeader(props) {
    const style = {
      header: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
        heigth: "10em",
         backgroundColor:"#179be8",
        
      },
      title: {
        alignSelf: "center",
        fontFamily: "Arial",
        color: "red",
        paddingRight: "1em",
      },
      logo: {
        height: "10em",
      },
    };
    
    return (
      <header style={style.header}>
        <img alt={`Logo ${props.title}`} src={props.image} style={style.logo} />
        <h1 style={style.title}>{props.title}</h1>
      </header>
    );
}

class ArticleSummary extends React.Component {
    style = {
        display: "flex",
        flexDirection: "column",
        width: "10em",
        margin:"5px "
    }
    
    showDetail = () => { this.props.action(this.props.item) }
    

    render() {
        
       
        return (<article style={this.style} key={this.props.item.Id}>
            <ImageSummary name={this.props.item.Title} image={this.props.item.imageTh} />
            <AButton caption={this.props.item.Title} onClick={this.showDetail} />
        </article>);
    }
}

function LeafLetNode() {
    const style = {
        width: "40em",
        height: "40em",
        flex:"1"
    }
    return (<div id="leaflet-id" style={style}></div>)
}

class ArticleDetail extends React.Component {
    style = {
        detail: {
            display: "flex",
            flexDirection: "column",
            width: "70%"
        },
        top: {
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap"  
        },
        middle: {
            display: "flex",
            flexDirection: "row"
        },
        comment: {
            display: "block"
        }
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.leafLetView('leaflet-id');
    }

    showOverview = () => { this.props.actionOverview() }

    leafLetView = (id) => {
        // Initialiseert de mijnKaart constante
        const map = L.map(id);
        // Laad de basiskaart
        const baseMap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        });
        // plaats de basismap in het html element met id = kaart-id
        baseMap.addTo(map);
        // Stel het gebied van de kaart in
        // de laatste parameter bepaalt de grootte van het getoonde gebied
        // hoe groter het getal, hoe gedetailleerder
        // alert(this.props.article.longitude);
        const pos1 = this.props.article.latitude.substr(0, 5);
        const pos2 = this.props.article.longitude.substr(0, 5);
        const mark = L.marker([this.props.article.latitude, this.props.article.longitude]).addTo(map);
        // alert(pos1 + pos2);
        map.setView([pos2, pos1], 12);
    }

    render() {
        return (
          <article style={this.style}>
            <AButton
              caption="Terug naar overzicht"
              onClick={this.showOverview}
            />
            <div id="first" style={this.style.top}>
              <ImageDetail
                style={this.style.top}
                name={this.props.article.Title}
                image={this.props.article.image1}
                article={this.props.article}
              />
              <LeafLetNode />
            </div>
            <div style={this.style.middle}>
              <div className="description">
                <ul>
                  <li>Type: {this.props.article.Type}</li>
                  <li>loacationSpec:{this.props.article.loacationSpec}</li>
                  <li>room_link:{this.props.article.room_link}</li>
                  <li>beds:{this.props.article.beds}</li>
                  <li>location:{this.props.article.adress}</li>
                  <li>reviews:{this.props.article.review}</li>
                </ul>
                <div className="likepanel">
                  <LikePanel keyValue={this.props.article.Id} />
                </div>
                <div className="comments">
                  <Comments keyValue={parseInt(this.props.article.Id)} username={username} />
                </div>
              </div>
            </div>
            
          </article>
        );
    }
}

function ArticleOverview(props) {
    
    const style = {
        color: 'white',
        display: 'flex',
        flexWrap: 'wrap'
    };
    // alert(JSON.stringify(props));
   
    return (
        <main>
            <div style={style}>
                {props.articles.map(itemn => (<ArticleSummary item={itemn} key={itemn.Id} action={props.action} />))}
            </div>
        </main>
    );
}
const GOOGLE_BUTTON_ID = "google-sign-in-button";
var username = "anonymous";

class GoogleSignIn extends React.Component {
  componentDidMount() {
    window.gapi.signin2.render(GOOGLE_BUTTON_ID, {
      width: 200,
      height: 50,
      onsuccess: this.onSuccess,
    });
  }

  onSuccess(googleUser) {
    const profile = googleUser.getBasicProfile();
    username = profile.getName();
    
  }

  render() {
    return <div id={GOOGLE_BUTTON_ID} />;
  }
}
class ArticleApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      overview: true,
      
    };
  }
 /*  addusername = (name) => {
    this.setState({
      article: name,
    });
  }; */
  showDetail = (item) => {
    this.setState({
      overview: false,
      article: item,
    });
  };

  showOverview = () => {
    this.setState({
      overview: true,
      article: null,
    });
  };
  

  render() {
    
    if (this.state.overview) {
      return (
        <>
       <GoogleSignIn/>
        <ArticleOverview
          articles={this.props.articles}
          action={this.showDetail}
          
        /> </>
      );
    } else {
      return (
        <>
          <GoogleSignIn />
          <ArticleDetail
            article={this.state.article}
            actionOverview={this.showOverview}
          />
        </>
      );
    }
  }
}

