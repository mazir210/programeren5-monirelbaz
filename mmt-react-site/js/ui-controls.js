function ALabel(props) {
  const style = {
    display: "inline-block",
    color: "#ffffff",
    backgroundColor: "#3d94f6",
    textShadow: "0px 1px 0px #1570cd",
    padding: "6px 24px",
    textShadow: "0px 1px 0px #1570cd",
    borderRadius: "6px",
    border: "1px solid #337fed",
  };
  return (
    <div>
      <label style={style}>{props.caption}</label>
    </div>
  );
}

function AButton(props) {
  const style = {
    flex: 1,
    boxShadow: "inset 0px 1px 0px 0px #97c4fe",
    background: "linear-gradient(to bottom, #3d94f6 5%, #1e62d0 100%)",
    backgroundColor: "#3d94f6",
    borderRadius: "6px",
    border: "1px solid #337fed",
    display: "inline-block",
    cursor: "pointer",
    color: "#ffffff",
    fontFamily: "Arial",
    fontSize: "1em",
    fontWeight: "bold",
    padding: "6px 24px",
    textShadow: "0px 1px 0px #1570cd",
  };

  return (
    <button onClick={props.onClick} style={style}>
      {typeof props.caption !== "undefined" ? props.caption : "button"}
    </button>
  );
}

function ImageSummary(props) {
  const style = {
    borderRadius: "6px",
  };
  return (
    <img style={style} alt={`afbeelding van ${props.name}`} src={props.image} />
  );
}

function ImageDetail(props) {
  const style = {
    // width: "25em",
    flex: "1 ",
   
  };


  return (
 <img style={style} alt={`afbeelding van ${props.name}`} src={props.image} />


  );
}

function UnorderedList(props) {
  return (
    <ul>
      {props.ul.map((item) => (
        <li key={item.key}>{item.key}</li>
      ))}
    </ul>
  );
}

class LikePanel extends React.Component {
  host = "http://localhost:5000/MmtLike";

  constructor(props) {
    super(props);
    this.state = {
      likes: 0,
    };
    this.getLikes();
  }
  getLikes = () => {
    const keyValue = this.props.keyValue;

    let count = 0;
    if (typeof keyValue !== "undefined") {
      // als de callback wordt uitgevoerd, is this niet meer in de scope
      // daarom slaan we die op in een constante en geven die met de callback mee
      const self = this;
      const url = `${this.host}/${keyValue}`;
      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          this.setState({
            likes: data.likes,
          });
        });
    }
  };
  postLikes = () => {
    let item = {
      Key: this.props.keyValue,
      Name: "onbelangrijk",
      Likes: 1,
    };

    postData(this.host, item).then((data) => {
      this.setState({
        likes: data.likes,
      });
    });
  };
  incrementLike = () => {
    const keyValue = this.props.keyValue;
    if (typeof keyValue !== "undefined") {
      this.postLikes();
    } else {
      let newCount = this.state.likes + 1;
      this.setState({
        likes: newCount,
      });
    }
  };

  render() {
    return (
      <div>
        <AButton caption="I like" onClick={this.incrementLike} />
        <ALabel caption={this.state.likes} />
      </div>
    );
  }
}
const todoitemcontainer = {
  background: "#fff",
  borderRadius: "8px",
  marginTop: "8px",
  padding: "16px",
  position: "relative",
  boxShadow: "0 4px 8px grey",
};
const addCommentButtonStyle ={
  fontSize:"12px", 
  padding:"8px", "border":"none", "borderRadius":"8px", "outline":"none", "cursor":"pointer", "marginLeft":"8px", "width":"20%", "backgroundColor":"#22ee22"
};
const new_comment_input = {
  fontSize: "16px",
  padding: "8px",
  border: "none",
  borderBottomColor: "currentcolor",
  borderBottomStyle: "none",
  borderBottomWidth: "medium",
  borderBottom: "2px solid #ddd",
  borderRadius: "8px",
  width: "70%",
  outline: "none",
};
class Comments extends React.Component {
  style = {
    todoitemcontainer: {
      background: "#fff",
      borderRadius: "8px",
      marginTop: "8px",
      padding: "16px",
      position: "relative",
      boxShadow: "0 4px 8px grey",
    },
    addCommentButtonStyle: {
      fontSize: "12px",
      padding: "8px",
      border: "none",
      borderRadius: "8px",
      outline: "none",
      cursor: "pointer",
      marginLeft: "8px",
      width: "20%",
      backgroundColor: "#22ee22",
    },
    new_comment_input: {
      fontSize: "16px",
      padding: "8px",
      border: "none",
      borderBottomColor: "currentcolor",
      borderBottomStyle: "none",
      borderBottomWidth: "medium",
      borderBottom: "2px solid #ddd",
      borderRadius: "8px",
      width: "70%",
      outline: "none",
    },
    comment: {
      display: "block",
    },
  };

  host = "http://localhost:5000/mmtComment";
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      username: props.username,
      commentText: "",
    };
    this.getComments();
  }

  getComments = () => {
    const keyValue = this.props.keyValue;
   
    
    
    if (typeof keyValue !== "undefined") {
     
      const self = this;
      const url = `${this.host}/${keyValue}`;

      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          console.log(data)
          this.setState({
            comments: data,
           
          });
        });
    }
  };
  addComment = () => {
    const keyValue = this.props.keyValue;
    let item = {
      Key: keyValue,
      name: this.state.username,
      comment: this.state.commentText,
    };

    postData("http://localhost:5000/MmtComment", item).then((data) => {
      this.setState({
        comments: data,
        username: "anoymous",
        commentText: "",
      });
    });
  };
  addCommentsField = () => (
    <div id="post-status" style={this.style.todoitemcontainer}>
      <label>
        <input
          type="text"
          className="new_comment_input"
          style={this.style.new_comment_input}
          rows="4"
          cols="50"
          value={this.state.commentText}
          onChange={(event) =>
            this.setState({
              commentText: event.target.value,
            })
          }
        />
      </label>
      <button onClick={this.addComment} style={this.style.addCommentButtonStyle}>
        Add Comment
      </button>
    </div>
  );
  commnetsList = ({ comment }) => (
    <div className="todo-item-container" style={this.style.todoitemcontainer}>
      <p>
        {comment.comment} 
      </p>
      <span>by: {comment.name} </span>
    </div>
  );

  render() {
    return (
      <div className="react-statusmanager">
        <this.addCommentsField />
        <h3>Comments:</h3>
        {this.state.comments.map((comment) => (
          <this.commnetsList comment={comment} key={comment.id} />
        ))}
      </div>
    );
  }
}
