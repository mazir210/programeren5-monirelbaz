﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MmpApi.Models;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MmpApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MmtCommentController : ControllerBase
    {
        private readonly MmtContext mmtContext;
        // GET: /<controller>/
        public MmtCommentController(MmtContext context)
        {
            mmtContext = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<MmtComment>>> GetMmtComments()
        {
         
            return await mmtContext.MmtComments.ToListAsync() ;

        }





        [HttpGet("{key}")]
        public async Task<ActionResult<IEnumerable<MmtComment>>> GetMmtComments(int key)
        {
            
            return await mmtContext.MmtComments.Where(a => a.Key == key).ToListAsync();

        }


        

        [HttpPost]
        

        public async Task<ActionResult<IEnumerable<MmtComment>>> MmtComment(MmtComment mmtComment)
        {
            
            mmtContext.MmtComments.Add(mmtComment);
            await mmtContext.SaveChangesAsync();
            var key = mmtComment.Key;
            
            //return CreatedAtAction("GetComments", new { id = mmtComment.Id }, mmtComment);
            return await mmtContext.MmtComments.Where(a => a.Key == mmtComment.Key).ToListAsync(); ;
        }

        

    }
}
