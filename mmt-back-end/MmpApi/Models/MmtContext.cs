﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MmpApi.Models
{
    public class MmtContext :DbContext
    {
        public MmtContext(DbContextOptions<MmtContext> options)
                : base(options)
        {
        }

        public DbSet<MmtLike> MmtLikes { get; set; }
        public DbSet<MmtComment> MmtComments { get; set; }
        public object MmtComment { get; internal set; }
    }
}
